# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2010-2016 David Vo <david.vo2@gmail.com>
# Copyright (C) 2011 aLaserShark <shark@alasershark.com>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.


#import urllib.request, urllib.parse, urllib.error, json
from urllib.request import Request, urlopen
import json


@plugin
class IsItUp(object):
    """Check domain status using isitup.org"""
    def __init__(self, server):
        self.server = server
        self.prnt = server.prnt
        self.commands = ["isitup"]
        self.server.handle("command", self.handle_command, self.commands)

    def handle_command(self, channel, user, cmd, args):
        if len(args) < 1:
            self.server.doMessage(channel, user + ": Not enough arguments.")
            return

        if args[0].startswith("http://"):
            url = args[0][7:]
        elif args[0].startswith("https://"):
            url = args[0][8:]
        else:
            url = args[0]
        if url.endswith("/"):  #check if URL has a trailing slash
            url = url[:-1]     #and remove it if needed   
        url = "http://isitup.org/" +url + ".json"
        req = Request(url,headers={'User-Agent': 'guppy/'+self.server.config["version"]})
        code = "".join(urlopen(req).read().decode('utf8'))
        arr = json.loads(code)
        arr['up'] = 'up' if arr['status_code'] == 1 else 'not up'
        if arr['up'] == 'up':
            self.server.doMessage(channel, user + ': %(domain)s:%(port)s is %(up)s. Got HTTP status code %(response_'
                                                  'code)03d from %(response_ip)s in %(response_time)f s.' % arr)
        else:
            self.server.doMessage(channel, user + ': %s is not up.' % arr['domain'])
