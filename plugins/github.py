__author__ = 'alasershark'

# Copyright (C) 2010-2016 aLaserShark <a.laser.sharky@gmail.com>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.

import urllib
import json

@plugin
class Github(object):

    """
    Returns the latest git commit in a given github repository.
    """

    def __init__(self, server):
        self.commands = ['github']
        self.server = server
        self.server.handle("command", self.handle_command, self.commands)

    def handle_command(self, channel, usr, command, args):
        if command == "github":
            req = urllib.request.Request("https://api.github.com/repos/" + str(args[0]) + "/commits")
            # self.server.doMessage(channel, "https://api.github.com/repos/" + str(args[0]) + "/commits")
            data = urllib.request.urlopen(req).read().decode('utf-8')
            # self.server.doMessage(channel, data)
            json_data = json.loads(data)[0]
            commit = json_data['commit']
            author = commit['author']
            self.server.doMessage(channel, "The last commit was by " + author['name'] + " on " +
                                           author['date'].split('T')[0] + " and the message was " + commit['message'])
