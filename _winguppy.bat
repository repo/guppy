@REM Copyright (C) 2016 Nicolas Aragone <niquito.larigone@gmail.com>

@REM This file is part of guppy.

@REM guppy is free software: you can redistribute it and/or modify
@REM it under the terms of the GNU General Public License as published by
@REM the Free Software Foundation, either version 3 of the License, or
@REM (at your option) any later version.

@REM guppy is distributed in the hope that it will be useful,
@REM but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@REM GNU General Public License for more details.
@REM You should have received a copy of the GNU General Public License
@REM along with guppy.  If not, see <http://www.gnu.org/licenses/>.

@REM Note: Requires python 3 installed and available in PATH


@start python guppy.py